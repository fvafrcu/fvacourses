[![pipeline status](https://gitlab.com/foobar/fvaCourses/badges/master/pipeline.svg)](https://gitlab.com/foobar/fvaCourses/commits/master)    
[![coverage report](https://gitlab.com/foobar/fvaCourses/badges/master/coverage.svg)](https://gitlab.com/foobar/fvaCourses/commits/master)
<!-- 
    [![Build Status](https://travis-ci.org/foobar/fvaCourses.svg?branch=master)](https://travis-ci.org/foobar/fvaCourses)
    [![Coverage Status](https://codecov.io/github/foobar/fvaCourses/coverage.svg?branch=master)](https://codecov.io/github/foobar/fvaCourses?branch=master)
-->
[![CRAN_Status_Badge](https://www.r-pkg.org/badges/version/fvaCourses)](https://cran.r-project.org/package=fvaCourses)
[![RStudio_downloads_monthly](https://cranlogs.r-pkg.org/badges/fvaCourses)](https://cran.r-project.org/package=fvaCourses)
[![RStudio_downloads_total](https://cranlogs.r-pkg.org/badges/grand-total/fvaCourses)](https://cran.r-project.org/package=fvaCourses)

<!-- README.md is generated from README.Rmd. Please edit that file -->

```{r, echo = FALSE}
knitr::opts_chunk$set(
  collapse = TRUE,
  comment = "#>",
  fig.path = "README-"
)
```

# fvaCourses
## Introduction
Please read the
[vignette](https://gitlab.com/foobar/fvaCourses/raw/master/doc/An_Introduction_to_fvaCourses.html).
<!-- 
[vignette](https://CRAN.R-project.org/package=fvaCourses/vignettes/An_Introduction_to_fvaCourses.html).

-->

Or, after installation, the help page:
```{r, eval = FALSE}
help("fvaCourses-package", package = "fvaCourses")
```
```{r, echo = FALSE}
  # insert developement page
  help_file <- file.path("man", "fvaCourses-package.Rd")
  captured <- gsub('_\b', '',  capture.output(tools:::Rd2txt(help_file) ))
  cat(captured, sep = "\n")
```

## Installation

You can install fvaCourses from gitlab with:

```{r gh-installation, eval = FALSE}
if (! require("remotes")) install.packages("remotes")
remotes::install_gitlab("foobar/fvaCourses")
```


