#' Create Course Directories
#' 
#' Creates a course folder for every participant and adds an html link to the
#' course script.
#' @param participants A \code{\link{data.frame}} containing the participants specifications.
#' Typically the output of \code{\link{get_participants}}.
#' @param output_path The root for the course folders. Will be created on disk!
#' @param force Recursively \code{\link{unlink}} \code{output_path} first?
#' @return This function is called for its side effect, but, invisibly, a 
#' \code{\link{data.frame}} consisting of:
#' \itemize{ 
#'    \item path. The path created.
#'    \item create. The return values of the path creation.
#'    \code{path}.
#' }
#' 
#' @examples
#' p <- get_participants(course_folder = "\\Posteingang\\Kurse\\pir_2019\\kurs1",
#'                       max_participants = 12,
#'                       outlook_app = COMCreate("Outlook.Application"),
#'                       inbox = "Dominik.Cullmann@Forst.bwl.de")
#' a <- create_course_directories(p)
#' print(a)      
#' @export
create_course_directories <- function(participants,
                                      output_path = file.path("k:", "transpor", "programmieren_in_r"),
                                      force = TRUE) {
    dir_names <- paste(participants[["given_name"]], 
                       participants[["family_name"]], sep = "_")
    
    if(isTRUE(force)) unlink(output_path, recursive = TRUE)
    
    status_index <- NULL
    status_directory <- NULL
    personal_directories <- NULL
    for (dir_name in dir_names) {
        personal_directory <- file.path(output_path, dir_name)
        personal_directories <- c(personal_directories, personal_directory)
        status_directory <- c(status_directory, 
                              dir.create(personal_directory, recursive = TRUE))
        writeLines(get_html_code(), file.path(personal_directory, 
                                              "programmieren_in_r.html"))
    }
    
    return(invisible(data.frame(path = personal_directories, 
                                create = status_directory)))
}

get_html_code <- function() {
    c("<html>", "<head>", "<title></title>", 
      "<meta http-equiv=\"refresh\" content=\"2; URL=https://fvafrcu.gitlab.io/programmieren_in_r/\">", 
      "<meta name=\"keywords\" content=\"automatic redirection\">", 
      "</head>", "<body>", 
      "If your browser doesn't automatically go there within a few seconds, ", 
      "you may want to go to ", 
      "<a href=\"https://fvafrcu.gitlab.io/programmieren_in_r/\">the destination</a> ", 
      "manually.", "</body>", "</html>")
}
